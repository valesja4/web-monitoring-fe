# MĚŘENÍ DOSTUPNOSTI WEBU

Aplikace vznikla jako UI pro službu měření dostupnosti webových stránek, kterou dělám v předmětu SWA.

Pro vývoj jsem zvolil React, za účelem osvojení nové technologie.

## Spuštění
`npm install`
`npm start`

### Popis aplikace
Účelem aplikace je poskytnout uživatelům rozhraní pro správu URL. Na hlavní stránce je input pro přidání nového projektu společně s přehledem existujících projektů.

#### Projekt
Na detailu projektu jsou vidět souhrnná data. Projekt je zastřešuje několik měřených URL a zobrazuje jejich souhnná (dummy) data.

Do projektu lze vložit libovolný počet URL k měření.

#### URL
Na detailu URL jsou vidět naměřená (dummy) data pro konkrétní URL.

Pro každou URL lze nastavit frekvenci měření a očekávané statusové kódy - pokud stránka vrátí jiný stavový kód, bude zaznamenán incident.
