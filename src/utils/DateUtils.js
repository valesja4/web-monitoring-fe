export function GetDateDifference(startDate, endDate, t) {
    const msPerMinute = 60 * 1000;
    const msPerHour = msPerMinute * 60;
    const msPerDay = msPerHour * 24;

    const elapsed = endDate - startDate;

    if (elapsed < msPerMinute) {
        const seconds = Math.round(elapsed / 1000);
        return t("seconds", { count: seconds });
    } else if (elapsed < msPerHour) {
        const minutes = Math.round(elapsed / msPerMinute);
        return t("minutes", { count: minutes });
    } else if (elapsed < msPerDay) {
        const hours = Math.round(elapsed / msPerHour);
        return t("hours", { count: hours });
    } else {
        const days = Math.round(elapsed / msPerDay);
        return t("days", { count: days });
    }
}
