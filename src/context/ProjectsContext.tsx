import React, {createContext, useReducer, useContext, ReactNode, useEffect} from 'react';

export interface Incident {
    id: number;
    url: string;
    start: Date;
    end?: Date;
    statusCode: number;
}

export interface Url {
    id: number;
    url: string;
    settings: UrlSettings;
    incidents: Incident[];
    deleted: boolean;
}

export interface UrlSettings {
    measurementFrequency: number;
    expectedStatusCodes: number[];
}

export interface Project {
    id: number;
    name: string;
    urls: Url[];
    nextUrlId: number;
    deleted: boolean;
}

export interface ProjectState {
    projects: Project[];
    nextProjectId: number;
}

export interface ProjectContextProps {
    state: ProjectState;
    addProject: (project: string) => void;
    addUrl: (projectId: number, url: string) => void;
    deleteProject: (projectId: number) => void;
    deleteUrl: (projectId: number, urlId: number) => void;
    updateUrlSettings: (projectId: number, urlId: number, settings: UrlSettings) => void;
}

interface ProjectProviderProps {
    children: ReactNode;
}

export const ProjectContext = createContext<ProjectContextProps | undefined>(undefined);

type Action =
    | { type: 'ADD_PROJECT'; payload: Project }
    | { type: 'ADD_URL'; projectId: number; payload: Url }
    | { type: 'DELETE_PROJECT'; projectId: number }
    | { type: 'DELETE_URL'; projectId: number; urlId: number }
    | { type: 'UPDATE_URL_SETTINGS'; projectId: number; urlId: number; payload: UrlSettings };

const initialState = {
    projects: [],
    nextProjectId: 0
};

const LOCAL_STORAGE_KEY = 'projects';

const projectReducer = (state: ProjectState, action: Action): ProjectState => {
    switch (action.type) {
        case 'ADD_PROJECT':
            state.projects[action.payload.id] = action.payload;
            return {...state};
        case 'ADD_URL':
            state.projects[action.projectId].urls[action.payload.id] = action.payload;
            return {...state};
        case 'DELETE_PROJECT':
            state.projects[action.projectId].deleted = true;
            return {...state};
        case 'DELETE_URL':
            state.projects[action.projectId].urls[action.urlId].deleted = true;
            return {...state};
        case 'UPDATE_URL_SETTINGS':
            return {
                ...state,
                projects: state.projects.map(project =>
                    project.id === action.projectId
                        ? {
                            ...project,
                            urls: project.urls.map(url =>
                                url.id === action.urlId
                                    ? {...url, settings: action.payload}
                                    : url
                            )
                        }
                        : project
                )
            };
        default:
            return state;
    }
};

const loadStateFromLocalStorage = (): ProjectState => {
    try {
        const serializedState = localStorage.getItem(LOCAL_STORAGE_KEY);
        return serializedState ? JSON.parse(serializedState) : initialState;
    } catch (e) {
        console.warn('Could not load state from local storage', e);
        return initialState;
    }
};

const saveStateToLocalStorage = (state: ProjectState) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem(LOCAL_STORAGE_KEY, serializedState);
    } catch (e) {
        console.warn('Could not save state to local storage', e);
    }
};


export const ProjectProvider: React.FC<ProjectProviderProps> = ({children}) => {
    const [state, dispatch] = useReducer(projectReducer, initialState, loadStateFromLocalStorage);

    useEffect(() => {
        saveStateToLocalStorage(state);
    }, [state]);


    const addProject = (project: string) => {
        const projectObj = {id: state.nextProjectId, name: project, urls: [], nextUrlId: 0, deleted: false};
        state.nextProjectId++;
        dispatch({type: 'ADD_PROJECT', payload: projectObj});
    };

    const addUrl = (projectId: number, url: string) => {
        const settings = {measurementFrequency: 0, expectedStatusCodes: [200]};
        const urlObj = {id: state.projects[projectId].nextUrlId, url: url, settings, incidents: [], deleted: false};
        state.projects[projectId].nextUrlId++;
        dispatch({type: 'ADD_URL', projectId, payload: urlObj});
    };

    const updateUrlSettings = (projectId: number, urlId: number, settings: UrlSettings) => {
        dispatch({type: 'UPDATE_URL_SETTINGS', projectId, urlId, payload: settings});
    };

    const deleteProject = (projectId: number) => {
        dispatch({type: 'DELETE_PROJECT', projectId});
    }

    const deleteUrl = (projectId: number, urlId: number) => {
        dispatch({type: 'DELETE_URL', projectId, urlId});
    }

    return (
        <ProjectContext.Provider value={{state, addProject, addUrl, updateUrlSettings, deleteProject, deleteUrl}}>
            {children}
        </ProjectContext.Provider>
    );
};

export const useProjects = (): ProjectContextProps => {
    const context = useContext(ProjectContext);
    if (!context) {
        throw new Error('useProjects must be used within a ProjectProvider');
    }
    return context;
};
