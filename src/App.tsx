import React, {useState, useEffect} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Home from './pages/Home/Home';
import ProjectDetail from './pages/ProjectDetail/ProjectDetail';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {useTranslation} from 'react-i18next';
import {ProjectProvider} from "./context/ProjectsContext";
import UrlDetail from "./pages/UrlDetail/UrlDetail";

const App: React.FC = () => {
    const {t, i18n} = useTranslation();
    const [urls, setUrls] = useState<string[]>(() => {
        const storedUrls = localStorage.getItem('urls');
        return storedUrls ? JSON.parse(storedUrls) : [];
    });

    useEffect(() => {
        localStorage.setItem('urls', JSON.stringify(urls));
    }, [urls]);

    useEffect(() => {
        const storedLanguage = localStorage.getItem('language');
        if (storedLanguage) {
            i18n.changeLanguage(storedLanguage);
        }
    }, [i18n.language])

    const handleDeleteUrl = (url: string) => {
        setUrls(urls.filter(u => u !== url));
        toast.success(t('url_deleted'));
    };

    const changeLanguage = (lng: string) => {
        i18n.changeLanguage(lng).then(() => {
            console.log('Language changed to:', lng);
            localStorage.setItem('language', lng);
        }).catch(err => {
            console.error('Error changing language:', err);
        });
    };

    return (
        <Router>
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">{t('title')}</h1>
                    <div className="language-selector">
                        <button
                            className={i18n.language === 'en' ? 'active' : ''}
                            onClick={() => changeLanguage('en')}
                        >
                            EN
                        </button>
                        <button
                            className={i18n.language === 'cs' ? 'active' : ''}
                            onClick={() => changeLanguage('cs')}
                        >
                            CS
                        </button>
                    </div>
                </header>
                <main>
                    <ProjectProvider>
                        <Routes>
                            <Route path="/" element={<Home/>}/>
                            <Route path="/projects/:projectId" element={<ProjectDetail/>}/>
                            <Route path="/projects/:projectId/urls/:urlId" element={<UrlDetail/>}/>
                        </Routes>
                    </ProjectProvider>
                </main>
                <ToastContainer position="top-right" autoClose={2000} hideProgressBar={true} closeOnClick
                                pauseOnHover
                                draggable/>
            </div>
            <svg width="100%" height="100%" viewBox="0 0 300 200" xmlns="http://www.w3.org/2000/svg"
                 preserveAspectRatio="none" style={{position: 'fixed', top: 0, left: 0, zIndex: -1}}>
                <rect width="100%" height="100%" fill="none" />
                <rect x="0" y="0" width="100%" height="100%" fill="#1f1f1f" />
                <path d="M0 100 C 75 50, 225 150, 300 100 L 300 200 L 0 200 Z" fill="#242a3a" />
                <path d="M0 130 C 75 80, 225 180, 300 130 L 300 200 L 0 200 Z" fill="#2a3347" />
                <path d="M0 160 C 75 110, 225 210, 300 160 L 300 200 L 0 200 Z" fill="#2f3c54" />
            </svg>
        </Router>
    );
};

export default App;
