import React, {useState} from 'react';
import Card from "../Ui/Card/Card";
import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {LineChart, CartesianGrid, Legend, Line, ResponsiveContainer, XAxis, YAxis, Tooltip} from "recharts";

const generateLastWeekDates = () => {
    const data = [];
    const now = new Date();
    // Počet hodin v týdnu (7 dní * 24 hodin)
    const hoursInWeek = 7 * 24;

    for (let i = 0; i < hoursInWeek; i++) {
        // Vytvoření nového data, které je o 'i' hodin zpět od aktuálního času
        const date = new Date(now.getTime() - i * 60 * 60 * 1000);
        data.push(date);
    }

    // Reverzace pole, aby byla data seřazena od nejstaršího po nejnovější
    return data.reverse();
};
const generateLastWeekData = () => {
    const dates = generateLastWeekDates();
    return dates.map(date => ({
        time: date.toISOString(),
        responseTime: Math.floor(Math.random() * 100),
    }));
};
const ResponseTimeInfo = () => {

    const {t} = useTranslation();

    const [responseTimes, setResponseTimes] = useState(generateLastWeekData());

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} md={4}>
                <Card>
                    {t('average_response_time')}
                    <div style={{marginTop: "16px", fontSize: "32px"}}>320 ms</div>
                </Card>
            </Grid>
            <Grid item xs={12} md={8}>
                <Card>
                    {t('response_time')}
                    <ResponsiveContainer width="100%" height={300}>
                        <LineChart data={responseTimes}>
                            <CartesianGrid
                                vertical={false}
                                strokeDasharray="0 0"
                                stroke="#333"
                            />
                            <XAxis
                                dataKey="time"
                                tickFormatter={(tick) => {
                                    const date = new Date(tick);
                                    return date.toLocaleDateString('cs-cz');
                                }}
                                interval={40}
                            />
                            <YAxis
                                tickCount={5}
                                axisLine={false}
                                tickLine={false}
                                stroke="#e0e0e0"
                            />
                            <Tooltip labelFormatter={(label) => new Date(label).toLocaleString()}/>
                            <Legend/>
                            <Line type="monotone" dataKey="responseTime" unit=" ms" name={t("response_time")} stroke="#21a1f1" dot={false}/>
                        </LineChart>
                    </ResponsiveContainer>
                </Card>
            </Grid>
        </Grid>
    );
};

export default ResponseTimeInfo;
