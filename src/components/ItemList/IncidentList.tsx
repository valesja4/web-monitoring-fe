import React from "react";
import {Incident} from "../../context/ProjectsContext";
import ItemList, {Item} from "./ItemList";
import {Chip, Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {GetDateDifference} from "../../utils/DateUtils";

interface IncidentListProps {
    incidents: Incident[]
}

const IncidentList: React.FC<IncidentListProps> = ({incidents}) => {
    const {t} = useTranslation();

    const incidentItems: Item[] = incidents.map((incident: Incident) => {
        const duration = incident.end ? GetDateDifference(incident.start, incident.end, t) : t("unresolved");
        return {
            id: 0, children: (
                <Grid container alignItems="center" spacing={3}>
                    <Grid item>
                        <Chip color="error" label={incident.statusCode}></Chip>
                    </Grid>
                    <Grid item>
                        {incident.url}
                    </Grid>
                    <Grid item>
                        {incident.start.toLocaleString('cs')}
                    </Grid>
                    <Grid item style={{marginLeft: 'auto'}}>
                        <Chip color="warning" label={duration} size="small"></Chip>
                    </Grid>
                </Grid>
            )
        }
    });

    return (
        <ItemList items={incidentItems}></ItemList>
    )
};

export default IncidentList;
