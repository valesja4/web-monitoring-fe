import React from 'react';
import './ItemList.css';
import { useTranslation } from 'react-i18next';

export interface Item {
    children: React.ReactNode,
    id: number
}

interface UrlListProps {
    items: Item[];
    onItemClick?: (id: number) => void;
}

const ItemList: React.FC<UrlListProps> = ({ items, onItemClick }) => {
    return (
        <div className="item-list">
            <ul>
                {items.map((item, index) => (
                    <li
                        key={index}
                        onClick={() => {onItemClick && onItemClick(item.id)}}
                    >
                        {item.children}
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default ItemList;
