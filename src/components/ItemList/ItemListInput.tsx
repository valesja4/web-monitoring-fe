import React, {useRef} from 'react';
import './ItemListInput.css';
import {useTranslation} from 'react-i18next';
import {FaArrowRight} from "react-icons/fa6";

interface ItemListInput {
    onAddItem: (value: string) => void;
    placeholder: string;
}

const ItemListInput: React.FC<ItemListInput> = ({onAddItem, placeholder}) => {
    const inputRef = useRef<HTMLInputElement>(null);

    const handleAddItem = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        if (inputRef.current !== null) {
            onAddItem(inputRef.current.value);
        }
    };

    const onChange = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            onAddItem(event.currentTarget.value);
        }
    };

    return (
        <div className="item-input-container">
            <div className="item-input">
                <input
                    type="text"
                    onKeyDown={onChange}
                    placeholder={placeholder}
                    ref={inputRef}
                />
                <button onClick={handleAddItem}>
                    <FaArrowRight/>
                </button>
            </div>
        </div>
    );
};

export default ItemListInput;
