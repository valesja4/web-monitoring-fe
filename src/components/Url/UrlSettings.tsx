import React from 'react';
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import the CSS for react-confirm-alert
import './UrlSettings.css';
import {useTranslation} from 'react-i18next';
import FrequencyInput from "./FrequencyInput/FrequencyInput";
import StatusCodesInput from "./StatusCodesInput/StatusCodesInput";
import {Grid} from "@mui/material";


const UrlSettings: React.FC = ({}) => {
    const {t} = useTranslation();

    return (
        <div className="url-details">
            <Grid container spacing={3}>
                <Grid item xs={12} md={6}>
                    <FrequencyInput></FrequencyInput>
                </Grid>
                <Grid item xs={12} md={6}>
                    <StatusCodesInput></StatusCodesInput>
                </Grid>
            </Grid>
        </div>
    );
};

export default UrlSettings;
