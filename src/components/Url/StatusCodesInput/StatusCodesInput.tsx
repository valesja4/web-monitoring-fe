import {useTranslation} from "react-i18next";
import "./StatusCodesInput.css";
import React, {ChangeEvent, Key, useState} from "react";
import Select, {MultiValue} from "react-select";
import {useParams} from "react-router-dom";
import {useProjects} from "../../../context/ProjectsContext";

interface StatusCodeOption {
    value: number;
    label: string;
}

const StatusCodesInput: React.FC = ({}) => {
    const {t} = useTranslation();

    const {urlId, projectId} = useParams<{ projectId: string, urlId: string }>();
    const { state, updateUrlSettings} = useProjects();
    const project = state.projects[Number(projectId)];
    const url = project.urls[Number(urlId)];

    const [statusCodes, setStatusCodes] = React.useState<number[]>(url.settings.expectedStatusCodes);

    const handleSelect = (selected: MultiValue<StatusCodeOption>) => {
        let options = selected.map((option) => {
            return option.value;
        });

        let settings = url.settings;
        settings.expectedStatusCodes = options;

        updateUrlSettings(project.id, url.id, settings);
        setStatusCodes(options);
    }

    const statusCodesOptions = [
        {value: 200, label: '200 OK'},
        {value: 201, label: '201 Created'},
        {value: 400, label: '400 Bad Request'},
        {value: 401, label: '401 Unauthorized'},
        {value: 403, label: '403 Forbidden'},
        {value: 404, label: '404 Not Found'},
        {value: 500, label: '500 Internal Server Error'},
        {value: 502, label: '502 Bad Gateway'},
        {value: 503, label: '503 Service Unavailable'},
        {value: 504, label: '504 Gateway Timeout'},
    ];

    const selectedOptions = statusCodesOptions.filter(option => statusCodes.includes(option.value));

    return (
        <>
            <h3>{t('expected_status_codes')}</h3>
            <Select classNamePrefix="select-dark" isMulti placeholder='' value={selectedOptions} options={statusCodesOptions} onChange={handleSelect} />
        </>
    );
}

export default StatusCodesInput;