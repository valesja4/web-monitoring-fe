import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import "./FrequencyInput.css";
import {useParams} from "react-router-dom";
import {useProjects} from "../../../context/ProjectsContext";
import { TFunction } from "i18next";

interface FrequencyInputProps {
}

export const getFrequencyOptions = (t: TFunction<"translation", undefined>) => {
    return [
        t('frequency.minute'),
        t('frequency.five_minutes'),
        t('frequency.fifteen_minutes'),
        t('frequency.hour'),
    ];
}

const FrequencyInput: React.FC<FrequencyInputProps> = ({ }) => {
    const { t } = useTranslation();

    const {urlId, projectId} = useParams<{ projectId: string, urlId: string }>();
    const { state, updateUrlSettings} = useProjects();
    const project = state.projects[Number(projectId)];
    const url = project.urls[Number(urlId)];

    const [selectedOption, setSelectedOption] = useState<number>(url.settings.measurementFrequency);

    const handleSelect = (option: number) => {
        setSelectedOption(option);
        let settings = url.settings;
        settings.measurementFrequency = option;
        updateUrlSettings(project.id, url.id, settings);
    };

    const options = getFrequencyOptions(t);

    return (
        <div className="option-selector">
            <h3>{t("frequency")}</h3>
            <div className="button-container">
                {options.map((option, index) => (
                    <button
                        key={index}
                        onClick={() => handleSelect(index)}
                        className={`option-button ${selectedOption === index ? 'selected' : ''}`}
                    >
                        {option}
                    </button>
                ))}
            </div>
        </div>
    );
}

export default FrequencyInput;
