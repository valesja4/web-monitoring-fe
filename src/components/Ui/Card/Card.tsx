import './Card.css';
import React from "react";

interface CardProps {
    children: React.ReactNode
}

const Card: React.FC<CardProps> = ({children }) => {
    return (
        <div className={'container'}>
            {children}
        </div>
    );
};

export default Card;
