import React from "react";
import {Link} from "react-router-dom";
import './Button.css'

interface ButtonProps {
    children?: React.ReactNode;
    href?: string;
    onClick?: () => void;
    className?: string;
}

const Button: React.FC<ButtonProps> = ({children = [], href = null, onClick = () => {}, className = ""}) => {
    if (href) {
        return (
            <Link className="button" to={href}>{children}</Link>
        )
    }

    return (
        <div className={`button ${className}`} onClick={onClick}>
            {children}
        </div>
    );
};

export default Button;
