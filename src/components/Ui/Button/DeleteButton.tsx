import React, {ReactNode} from "react";
import Button from "./Button";
import DeleteIcon from '@mui/icons-material/Delete';

const DeleteButton: React.FC<{ children: ReactNode, onClick: () => void}> = ({children, onClick}) => {
    return (
        <Button className="delete" onClick={onClick}><DeleteIcon/>{children}</Button>
    );
}

export default DeleteButton
