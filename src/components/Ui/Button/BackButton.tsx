import React, {ReactNode} from "react";
import Button from "./Button";
import ArrowLeftIcon from "@mui/icons-material/ArrowLeft";

const BackButton: React.FC<{ children: ReactNode, href: string }> = ({children, href}) => {
    return (
        <Button href={href}><ArrowLeftIcon/>{children}</Button>
    );
}

export default BackButton
