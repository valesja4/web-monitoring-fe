import React, {useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import './ProjectDetail.css';
import {useTranslation} from 'react-i18next';
import ItemListInput from "../../components/ItemList/ItemListInput";
import ItemList from "../../components/ItemList/ItemList";
import ResponseTimeInfo from "../../components/ResponseTimeInfo/ResponseTimeInfo";
import Card from "../../components/Ui/Card/Card";
import {Incident, Url, useProjects} from "../../context/ProjectsContext";
import BackButton from "../../components/Ui/Button/BackButton";
import IncidentList from "../../components/ItemList/IncidentList";
import {Chip, Grid} from "@mui/material";
import RefreshIcon from '@mui/icons-material/Refresh';
import {getFrequencyOptions} from "../../components/Url/FrequencyInput/FrequencyInput";
import DeleteButton from "../../components/Ui/Button/DeleteButton";
import {confirmAlert} from 'react-confirm-alert';
import {toast} from "react-toastify";

const generateIncidents = (urls: Url[]): Incident[] => {
    return urls.map((url: Url): Incident[] => {
        return [
            {id: 0, url: url.url, start: new Date(2024, 6, 8, 17, 21, 0), end: undefined, statusCode: 500},
            {
                id: 1,
                url: url.url,
                start: new Date(2024, 6, 8, 15, 21, 0),
                end: new Date(2024, 6, 8, 15, 31, 5),
                statusCode: 500
            }
        ]
    }).flat(1);
};

const ProjectDetail: React.FC = ({}) => {
    const {projectId} = useParams<{ projectId: string }>();
    const {addUrl, state, deleteProject} = useProjects();
    const project = state.projects[Number(projectId)];
    const {t} = useTranslation();
    const navigate = useNavigate();

    useEffect(() => {
        if (project === undefined) {
            toast.warning(t("undefined_project"))
            navigate("/");
        }
    }, []);

    if (project === undefined) {
        return (<></>);
    }

    const handleOnUrlClick = (urlId: number) => {
        navigate(`urls/${encodeURIComponent(urlId)}`);
    };

    const addUrlDetail = (value: string) => {
        //Validate URL
        try {
            let url = new URL(value);
        } catch (err) {
            toast.error(t('invalid_url'));
            return;
        }
        toast.success(t('url_added'));
        addUrl(Number(projectId), value);
    }

    const handleDelete = () => {
        confirmAlert({
            title: t('confirmation'),
            message: t('confirmation_delete_project'),
            buttons: [
                {
                    label: t('yes'),
                    onClick: () => {
                        deleteProject(project.id);
                        toast.success(t('project_deleted'))
                        navigate("/");
                    }
                },
                {
                    label: t('no')
                }
            ]
        });
    }

    const frequencyOptions = getFrequencyOptions(t);
    const urlsObjs = state.projects[Number(projectId)].urls.filter((url) => {
        return !url.deleted
    });
    const urls = urlsObjs.map((url: Url) => {
        let urlLabel = (
            <>
                <div style={{display: "flex", alignItems: "center"}}><RefreshIcon
                    fontSize="small"/>{frequencyOptions[url.settings.measurementFrequency]}</div>
            </>
        );
        return {
            id: url.id, children: (
                <Grid container>
                    <Grid item>{url.url}</Grid>
                    <Grid item style={{marginLeft: "auto"}}><Chip color="primary" label={urlLabel} size="small"></Chip></Grid>
                </Grid>
            )
        }
    });

    return (
        <div className="url-management">
            <Grid container alignItems="center" justifyContent="space-between">
                <Grid item>
                    <BackButton href="/">{t('projects')}</BackButton>
                </Grid>
                <Grid item>
                    <h2 className="url-management-title">{project.name}</h2>
                </Grid>
                <Grid item>
                    <DeleteButton onClick={handleDelete}>{t('delete')}</DeleteButton>
                </Grid>
            </Grid>
            <ResponseTimeInfo/>
            <Card>
                {t('monitored_urls')}
                <ItemListInput onAddItem={addUrlDetail} placeholder={t('add_url')}/>
                <ItemList items={urls} onItemClick={handleOnUrlClick}/>
            </Card>
            <Card>
                <h3>{t('incidents')}</h3>
                <IncidentList incidents={generateIncidents(urlsObjs)}></IncidentList>
            </Card>
        </div>
    );
};

export default ProjectDetail;
