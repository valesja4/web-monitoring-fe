import React, {useEffect} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {Incident, useProjects} from "../../context/ProjectsContext";
import UrlSettings from "../../components/Url/UrlSettings";
import Card from "../../components/Ui/Card/Card";
import ResponseTimeInfo from "../../components/ResponseTimeInfo/ResponseTimeInfo";
import BackButton from "../../components/Ui/Button/BackButton";
import IncidentList from "../../components/ItemList/IncidentList";
import {Grid} from "@mui/material";
import DeleteButton from "../../components/Ui/Button/DeleteButton";
import {confirmAlert} from "react-confirm-alert";
import {toast} from "react-toastify";

const generateIncidents = (url: string): Incident[] => {
    return [
        {id: 0, url: url, start: new Date(2024, 6, 8, 17, 21, 0), end: undefined, statusCode: 500},
        {
            id: 1,
            url: url,
            start: new Date(2024, 6, 8, 15, 21, 0),
            end: new Date(2024, 6, 8, 15, 31, 5),
            statusCode: 500
        },
    ]
};

const UrlDetail: React.FC = ({}) => {
    const {urlId, projectId} = useParams<{ projectId: string, urlId: string }>();
    const {state, deleteUrl} = useProjects();
    const project = state.projects[Number(projectId)];
    const {t} = useTranslation();
    const navigate = useNavigate();

    useEffect(() => {
        if (project === undefined || project.urls[Number(urlId)] === undefined) {
            toast.warning(t("undefined_url"))
            navigate("/");
        }
    }, []);

    if (project === undefined || project.urls[Number(urlId)] === undefined) {
        return (<></>);
    }
    const url = project.urls[Number(urlId)];

    const handleDelete = () => {
        confirmAlert({
            title: t('confirmation'),
            message: t('confirmation_delete_url'),
            buttons: [
                {
                    label: t('yes'),
                    onClick: () => {
                        deleteUrl(project.id, url.id);
                        toast.success(t('url_deleted'))
                        navigate(`/projects/${project.id}`);
                    }
                },
                {
                    label: t('no')
                }
            ]
        });
    }

    return (
        <div className="url-management">
            <Grid container alignItems="center" justifyContent="space-between">
                <Grid item>
                    <BackButton href={"/projects/" + projectId}>{project.name}</BackButton>
                </Grid>
                <Grid item>
                    <h2 className="url-management-title">{url.url}</h2>
                </Grid>
                <Grid item>
                    <DeleteButton onClick={handleDelete}>{t('delete')}</DeleteButton>
                </Grid>
            </Grid>
            <Card>
                <UrlSettings></UrlSettings>
            </Card>
            <ResponseTimeInfo/>
            <Card>
                <h3>{t('incidents')}</h3>
                <IncidentList incidents={generateIncidents(url.url)}></IncidentList>
            </Card>
        </div>
    );
};

export default UrlDetail;
