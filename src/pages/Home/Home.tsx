import React from 'react';
import {useNavigate} from 'react-router-dom';
import './Home.css';
import {useTranslation} from 'react-i18next';
import ItemListInput from "../../components/ItemList/ItemListInput";
import ItemList from "../../components/ItemList/ItemList";
import {Project, useProjects} from "../../context/ProjectsContext";
import {toast} from "react-toastify";

const Home: React.FC = ({}) => {
    const {t} = useTranslation();
    const navigate = useNavigate();
    const {addProject, state} = useProjects();

    const handleOnProjectClick = (projectId: number) => {
        navigate(`projects/${encodeURIComponent(projectId)}`);
    };

    const projects = state.projects.filter((project) => {
        return !project.deleted
    }).map((project: Project) => {
        return {children: project.name, id: project.id};
    });

    const addProjectHome = (value: string) => {
        addProject(value);
        toast.success(t('project_added'));
    }

    return (
        <div className="home">
            <main>
                <h1>{t('projects')}</h1>
                <ItemListInput onAddItem={addProjectHome} placeholder={t('add_project')}/>
                <ItemList items={projects} onItemClick={handleOnProjectClick}/>
            </main>
        </div>
    );
};

export default Home;
